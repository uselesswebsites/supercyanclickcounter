var counter = 0;

function populateStorage() {
  localStorage.setItem('counter', counter);
  getSettingsFromStorage();
}

function getSettingsFromStorage() {
  counter = localStorage.getItem('counter');
  document.getElementById('counter').innerHTML = counter;
}

function incrementCounter() {
    counter++;
    populateStorage();
}

if(!localStorage.getItem('counter')) {
  populateStorage();
} else {
  getSettingsFromStorage();
}

document.onclick = incrementCounter;
